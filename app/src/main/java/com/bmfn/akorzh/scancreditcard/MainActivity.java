package com.bmfn.akorzh.scancreditcard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "Scan";
    private int MY_SCAN_REQUEST_CODE = 100;
    private TextView mResultTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mResultTv = (TextView)findViewById(R.id.result_tv);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onScanPress(view);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CardIOActivity.canReadCardWithCamera()) {
            mResultTv.setText("Scan a credit card");
        } else {
            mResultTv.setText("Enter credit card manually");
        }
    }


    private void onScanPress(View v) {

        Intent scanIntent = new Intent(this, CardIOActivity.class);

        /* customization*/

        //Frame color
        scanIntent.putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.BLUE); // default: Color.GREEN
        // hides the manual entry button
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false
        // matches the theme of your application
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, true); // default: false
        //Hide logo
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true); // default: false

        //If set, the user will be prompted for the card CVV.
        //scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false

        //If set, the user will be prompted for the card billing postal code.
        //scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, true); // default: false

        //f set, the postal code will only collect numeric input. Set this if you know the expected country's postal code has only numeric postal codes.
        //scanIntent.putExtra(CardIOActivity.EXTRA_RESTRICT_POSTAL_CODE_TO_NUMERIC_ONLY, false); // default: false

        //If set, the user will be prompted for the cardholder name.
        //scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true); // default: false

        //Used to display instructions to the user while they are scanning their card.
        //scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS, "Hello"); // default: false

        //If this value is provided the view will be inflated and will overlay the camera during the scan process.
        //The integer value must be the id of a valid layout resource.
        //scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_OVERLAY_LAYOUT_ID, R.layout.overlay);

        //How many of the Card number digits NOT to blur on the resulting image.
        //Setting it to 4 will blur all digits except the last four.*/
        //canIntent.putExtra(CardIOActivity.EXTRA_UNBLUR_DIGITS, -1); // default: -1

        //If set to false, expiry information will not be required.
        //scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false); // default: false

        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String resultStr;
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {

            CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            resultStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

            if (scanResult.isExpiryValid()) {
                resultStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
            }

            if (scanResult.cvv != null) {
                resultStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
            }

            if (scanResult.postalCode != null) {
                resultStr += "Postal Code: " + scanResult.postalCode + "\n";
            }

            if (scanResult.cardholderName != null) {
                resultStr += "Cardholder Name : " + scanResult.cardholderName + "\n";
            }
        } else {
            resultStr = "Scan was canceled.";
        }
        mResultTv.setText(resultStr);
    }
}
